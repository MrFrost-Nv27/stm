<?=$this->extend('Layout/Panel/main')?>
<?=$this->section('title')?><?=$title?> <?=$this->endSection()?>
<?=$this->section('main')?>

<div class="meta-card-wrapper"></div>


<style>
.meta-card-wrapper {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    gap: 1rem;
    width: 100%;
}

meta-item {
    position: relative;
    background-color: white;
    flex: 48%;
    border-radius: 1rem;
    padding: 1rem;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 0px 0.25rem;
    transition: all ease-in-out .3s;
    cursor: pointer;
    overflow: hidden;
}

.ajax-loading meta-item.loading {
    cursor: wait;
    pointer-events: none;
}

.ajax-loading meta-item::before {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    height: .25rem;
    background-color: var(--primary);
    animation: loadingline 2s infinite;
    opacity: 0;
    transition: opacity ease-in-out 0.5s;
}

.ajax-loading meta-item.loading::before {
    opacity: 1;
}

.ajax-loading meta-item.loading::after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.5);
}

meta-item a {
    text-decoration: none;
    color: inherit;
}

.meta-card:after {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: rgba(0, 0, 0, 0.35);
    top: 0;
    left: -50rem;
    z-index: 4;
    transition: all ease-in-out .3s;
}

meta-item p {
    margin: 0;
    padding: 0;
}

meta-item i {
    position: absolute;
    right: -10rem;
    top: 50%;
    padding: 0.875rem;
    margin-right: .5rem;
    background-color: var(--primary);
    color: white;
    transform: translateY(-50%);
    border-radius: 50px;
    transition: all ease-in-out .3s;
    z-index: 5;
}

.meta-card-title {
    font-size: 1rem;
    margin-bottom: 0.5rem;
}

.meta-card-desc {
    font-size: 0.75rem;
    text-align: justify;
    text-justify: inter-word;
    color: #666;
}

.meta-card-html p {
    font-size: 0.75rem;
    text-align: justify;
    text-justify: inter-word;
    color: #666;
}

.meta-card-image {
    width: 15rem;
    height: 8rem;
    margin: 0 auto;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

meta-item:not(.loading) .meta-card:hover {
    background-color: #f3f3f3;
    box-shadow: rgba(0, 0, 0, 0.5) 0px 0px 0.5rem;
}

.meta-card:active {
    transform: scale(1.025);
}

meta-item:not(.loading) .meta-card:hover i {
    right: 45%;
}

meta-item:not(.loading) .meta-card:hover:after {
    left: 0;
}

@media only screen and (min-width: 768px) {

    /* For desktop: */
    .meta-card-wrapper {
        flex-direction: row;
    }

    meta-item {
        max-width: 48%;
    }
}
</style>

<?=$this->endSection()?>