<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Panel | <?=$this->renderSection('title')?></title>
    <?=$this->include('Layout/Panel/_style')?>
</head>

<body>
    <?=$this->include('Layout/_popup')?>
    <?=$this->include('Layout/_preloader')?>
    <?=$this->include('Layout/_kop')?>

    <div class="panel-wrapper">
        <div class="menu-wrapper">
            <?=$this->include('Layout/Panel/_drawer')?>
        </div>
        <div class="content-wrapper">
            <div class="topbar">
                <?=$this->include('Layout/Panel/_topbar')?>
            </div>
            <div class="content-box">
                <div class="content">
                    <div class="content-title">
                        <h4><?=$title?></h4>
                        <small class="description"><?=$deskripsi ?? ""?></small>
                    </div>
                    <?=$this->renderSection('main')?>
                </div>
                <?=$this->include('Layout/Panel/_footer')?>
            </div>
        </div>
    </div>

    <?=$this->include('Components/init')?>
    <?=$this->include('Layout/Panel/_script')?>
</body>

</html>