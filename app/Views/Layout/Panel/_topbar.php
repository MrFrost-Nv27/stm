<div class="topbar-left">
    <div class="menu-toggle"><i class="fa-solid fa-chevron-right"></i></div>
</div>
<div class="topbar-right">
    <small class="me-2"><?=auth()->user()->nama ?? auth()->user()->username?></small>
    <div class="topbar-dropdown-wrapper">
        <div class="topbar-img" style="background-image: url('<?=auth()->user()->picture?>');"></div>
        <div class="topbar-dropdown">
            <a href="<?=route_to('logout') ?? "#!"?>" class="btn-logout"><i
                    class="fa-solid fa-right-from-bracket"></i>
                Keluar</a>
        </div>
    </div>
</div>