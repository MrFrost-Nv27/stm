<?=$this->include('Layout/_script')?>
<script src="<?=$url->api?>js/plugin/jquery.inview.min.js"></script>
<script src="<?=$url->api?>js/plugin/jquery-sortable-photos.js"></script>
<script src="<?=$url->api?>js/plugin/printThis.js"></script>

<script>
const Menus = JSON.parse(`<?=json_encode($menu ?? null)?>`);
</script>
<?=$this->renderSection('afterScript')?>