<img src="https://placehold.co/600x400" alt="Logo" width="150rem" class="logo">
<p class="menu-title">STM App</p>
<div class="menu-list-wrapper">
    <div class="menu-list menu-top">
        <ul>
        </ul>
    </div>
    <div class="menu-list menu-bottom">
        <ul>
            <li><a href="<?=route_to('logout') ?? "#!"?>" class="btn-logout"><i
                        class="fa-solid fa-right-from-bracket"></i>
                    Keluar</a></li>
        </ul>
    </div>
</div>