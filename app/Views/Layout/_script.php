<script>
const baseUrl = "<?=$url?>";
const UrlGenerator = JSON.parse(`<?=json_encode($url->all)?>`);
const current = "<?=$page ?? 'index'?>";
const currentPanel = "<?=$pagePanel ?? 'dashboard'?>";
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"
    integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"
    integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/3c5643e4eb.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="https://unpkg.com/dexie/dist/dexie.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/animsition/dist/js/animsition.min.js"
    integrity="sha256-8y2mv4ETTGZLMlggdrgmCzthTVCNXGUdCQe1gd8qkyM=" crossorigin="anonymous"></script>

<script>
let db = new Dexie("STMDatabase");
db.version(1).stores({
    meta: "++id, key, *value",
});
let store = {};
</script>
<script src="<?=$url->api?>js/panel.js"></script>

<?=$this->section('afterScript')?>
<script src="<?=base_url('script')?>/app.js" type="module"></script>
<?=$this->endSection()?>