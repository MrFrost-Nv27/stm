<div class="popup-wrapper close-button addon-button">
    <div class="popup-overlay"></div>
    <div class="popup-box animsition">
        <div class="popup-close"><i class="fa-solid fa-xmark"></i></div>
        <h4 class="popup-title">Title</h4>
        <div class="popup-content">
            <p>Content</p>
        </div>
        <div class="popup-button">
            <button class="gradient-button">Simpan</button>
        </div>
    </div>
</div>
