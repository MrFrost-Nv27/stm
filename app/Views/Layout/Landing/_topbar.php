<div class="topbar-menu">
    <a class="active" href="#beranda"><span class="material-symbols-outlined">home</span><span>Beranda</span></a>
    <a href="#tentang"><span class="material-symbols-outlined">info</span><span>Tentang</span></a>
</div>
<div class="topbar-logo">
    <img src="" alt="Logo">
</div>
<div class="topbar-menu">
    <a href="#jurusan"><span class="material-symbols-outlined">school</span><span>Jurusan</span></a>
    <a href="#biaya"><span class="material-symbols-outlined">receipt</span><span>Biaya</span></a>
</div>