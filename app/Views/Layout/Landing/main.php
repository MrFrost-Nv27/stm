<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$this->renderSection('title')?></title>
    <?=$this->include('Layout/Landing/_style')?>
</head>

<body>
    <?=$this->include('Layout/_preloader')?>
    <?=$this->include('Layout/_kop')?>

    <div id="topbar">
        <?=$this->include('Layout/Landing/_topbar')?>
    </div>

    <div class="hero-wrapper placeholder-glow" id="beranda">
        <?=$this->include('Layout/Landing/part_beranda')?>
    </div>

    <div>
        <div class="content-wrapper placeholder-glow" id="tentang">
            <?=$this->include('Layout/Landing/part_tentang')?>
        </div>

        <div class="content-wrapper placeholder-glow" id="jurusan">
            <?=$this->include('Layout/Landing/part_jurusan')?>
        </div>

        <div class="content-wrapper" id="biaya">
            <?=$this->include('Layout/Landing/part_biaya')?>
        </div>
    </div>

    <?=$this->include('Layout/Landing/_footer')?>
    <?=$this->include('Components/init')?>
    <?=$this->include('Layout/Landing/_script')?>
</body>

</html>