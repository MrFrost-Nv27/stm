<div class="content text-center">
    <div class="content-title">Biaya</div>
    <div class="content-box" data-aos="fade-up">
        <div class="table-responsive shadow" style="max-width: 700px; margin: 0 auto;">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Jenis Pembayaran</th>
                        <th scope="col">Nominal</th>
                    </tr>
                </thead>
                <tbody class="text-start">
                </tbody>
            </table>
        </div>
    </div>
    <a href="#!" role="button" class="btn btn-success btn-sm my-2 btn-print" data-print="#biaya"><span
            class="material-symbols-outlined">print</span></a>
</div>