<div class="hero placeholder">
    <div class="hero-welcome">
        <p>Welcome To</p>
        <p>SMK Al Hikmah 1</p>
        <p>Sirampog</p>
    </div>
</div>
<div class="hero-box">
    <div class="container text-center h-100">
        <div class="row h-100">
            <div class="col-8 d-none d-md-flex justify-content-start align-items-center">
                Ayo gabung bersama kami ! Bersekolah sambil mengaji
            </div>
            <div class="col d-flex justify-content-center align-items-center">
                <a href="https://alhikmahtajaga.cazh.id/ppdb/smk-1stm-al-hikmah-1" class="hero-btn">Daftar</a>
            </div>
        </div>
    </div>
</div>
