<div class="kop-wrapper text-center">
    <div class="d-flex">
        <img src="<?= $url->api ?>store/image/logo.png" alt="Logo">
        <div>
            <h4>Yayasan Pendidikan Pondok Pesantren Al Hikmah 1</h4>
            <h1>SMK Al Hikmah 1 Sirampog</h1>
            <h5>NSS : 4032915002 NPSN : 20326453</h5>
            <p>Alamat : Jl. Masjid Jami Alhikmah Benda Kec.Sirampog Kab.Brebes 52272 Jawa Tengah<br>
                Telp (0289) 5159163 / Email : smk_alhikmah1@yahoo.co.id Website:
                http://www.smkalhikmah1.sch.id
            </p>
        </div>
    </div>
</div>