<template id="post-item">
    <style>
        :host {
            display: block;
            width: 100%;
        }

        a {
            display: block;
            position: relative;
            background-color: white;
            margin: 0 10px 20px;
            overflow: hidden;
            border-radius: 25px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 0px 5px;
            text-decoration: none;
            color: inherit;
        }

        a::after {
            opacity: 0;
            content: "";
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.1);
            transition: all ease-in-out .3s;
        }

        a:hover::after {
            opacity: 1;
        }

        a:active::after {
            opacity: 1;
            background-color: rgba(0, 0, 0, 0.3);
        }

        #image {
            width: 100%;
            height: 200px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        #content {
            padding: 10px 20px;
        }

        #title {
            font-weight: bold;
            font-size: 1.05rem;
            text-align: justify;
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
        }

        #text {
            text-align: justify;
            font-size: 0.875rem;
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 4;
            -webkit-box-orient: vertical;
        }

        .wrapper {
            position: relative;
            width: 100%;
            height: 150px;
        }

        .meta {
            position: absolute;
            display: flex;
            justify-content: space-between;
            left: 0;
            right: 0;
            bottom: 0;
            font-size: .75rem;
            padding: 5px 15px;
            background-color: gainsboro;
        }

        .meta span {
            font-style: italic;
        }

        .posted-by span:nth-child(1) {
            display: none;
        }

        @media only screen and (min-width: 768px) {
            :host {
                width: 50%;
            }

            .posted-by span:nth-child(1) {
                display: inline;
            }
        }

        @media only screen and (min-width: 992px) {
            :host {
                width: 100%;
            }

            a {
                display: flex;
            }

            #image {
                max-height: 150px;
                max-width: 150px;
            }
        }
    </style>
    <a href="#!">
        <div id="image">
        </div>
        <div class="wrapper">
            <div id="content">
                <div id="title"></div>
                <div id="text"></div>
            </div>
            <div class="meta">
                <span class="date">20-05-2023</span>
                <span class="posted-by">
                    <span>Diposting Oleh : </span>
                    <span class="redakturs">Admin</span>
                </span>
            </div>
        </div>
    </a>
</template>