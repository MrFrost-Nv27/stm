<template id="list-post">
    <style>
        :host {
            display: block;
            position: relative;
            width: 100%;
            padding-bottom: 20px;
        }

        .post-button {
            display: none;
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: center;
        }

        a {
            text-decoration: none;
            background-color: var(--primary);
            color: white;
            padding: 0.25rem 0.5rem;
            font-size: 0.875rem;
            border-radius: 0.25rem;
            transition: all ease-in-out .3s;
        }

        a:hover {
            background-color: var(--primary-dark);
        }

        a:active {
            background-color: var(--primary-active);
        }

        .list-post {
            display: flex;
            flex-wrap: wrap;
        }
    </style>
    <div slot="list-post" class="list-post">
    </div>
    <div class="post-button">
        <a href="#!">Lihat Selengkapnya</a>
    </div>
</template>