<template id="jurusan-item-even">
    <style>
        :host {
            display: block;
            position: relative;
            width: 100%;
            color: white;
        }

        #image {
            display: none;
            position: absolute;
            width: 300px;
            height: 100%;
            background-color: red;
            left: auto;
            right: auto;
            bottom: auto;
            top: auto;
            background-color: var(--placeholder);
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            box-shadow: rgba(0, 0, 0, 0.4) 0px 0px 10px;
            border-top-left-radius: 120px;
            border-bottom-left-radius: 120px;
            right: 400px;
            transition: all ease-in-out .3s;
        }

        #box {
            position: relative;
            width: 78%;
            max-width: 440px;
            margin: 20px 0;
            padding: 10px 10px 20px;
            background-color: var(--primary);
            overflow: hidden;
            box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 15px;
            text-align: end;
            margin-left: auto;
            right: 0;
            border-top-left-radius: 120px;
            border-bottom-left-radius: 120px;
            padding-left: 50px;
        }

        #title {
            position: relative;
            font-weight: bold;
            font-size: 1rem;
            background-color: var(--primary) !important;
            margin-bottom: 30px;
            padding: 5px 10px;
            box-shadow: none;
        }

        #title::before,
        #title::after {
            position: absolute;
            bottom: -8px;
            content: "";
            background-color: white !important;
            height: 8px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 2px 5px;
        }

        #title::before {
            border-bottom-left-radius: 50px;
            border-bottom-right-radius: 50px;
            border-top-right-radius: 50px;
            border-top-left-radius: 0;
            left: 0px;
            width: calc(100% - 50px);
        }

        #title::after {
            border-bottom-right-radius: 50px;
            border-bottom-left-radius: 50px;
            border-top-left-radius: 50px;
            border-top-right-radius: 0;
            left: auto;
            right: 0px;
            width: 45px;
        }

        #desc {
            font-weight: 300;
            font-size: 0.875rem;
            text-align: justify;
            text-justify: inter-word;
        }

        @media only screen and (min-width: 992px) {
            #image {
                display: block;
            }
        }
    </style>
    <div id="image"></div>
    <div id="box">
        <div id="title">Teknik Permesinan</div>
        <div id="desc">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minus
            soluta
            voluptatum, voluptas doloribus repellat ratione.</div>
    </div>
</template>