<template id="social-media">
    <style>
    a {
        height: 20px;
        width: 20px;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        text-decoration: none;
        color: var(--primary);
        padding: 10px;
        border-radius: 50%;
        box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 4px;
        cursor: pointer;
        margin-bottom: 7px;
        transition: all ease-in-out 0.25s;
        background: linear-gradient(to left, white 50%, #f2f2f2 50%) right;
        background-size: 200%;
    }

    svg {
        max-height: 1.2rem;
        color: var(--primary);
    }

    a:hover {
        scale: 1.1;
        background-position: left;
    }

    a:active {
        scale: 0.9;
    }
    </style>
    <a href="#!" target="_blank"></a>
</template>