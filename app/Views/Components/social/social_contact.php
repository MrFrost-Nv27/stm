<template id="social-contact">
    <style>
    :host {
        display: block;
        width: 100%;
    }

    a {
        text-decoration: none;
        position: relative;
        height: auto;
        display: flex;
        align-items: center;
        margin-bottom: 10px;
        padding: 5px;
        padding-left: 60px;
        border-radius: 50px;
        background-color: var(--primary-dark);
        box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 4px;
        overflow: hidden;
        font-size: 0.8rem !important;
        color: white;
        transition: all ease-in-out .3s;
    }

    a:hover {
        scale: 1.025;
        background-position: left;
    }

    a:active {
        scale: 0.975;
    }

    #icon {
        position: absolute;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 200px;
        width: 50px;
        background-color: #f2f2f2;
        color: var(--primary);
    }

    svg {
        max-height: 1rem;
        color: var(--primary);
    }
    </style>
    <a href="#!" target="_blank">
        <div id="icon"></div>
        <span id="username"></span>
    </a>
</template>