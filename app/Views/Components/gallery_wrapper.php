<template id="gallery-wrapper">
    <style>
        :host {
            position: relative;
            display: block;
            width: 100%;
            max-height: 800px;
            overflow: hidden;
        }

        #wrapper {
            display: flex;
            flex-wrap: wrap;
            gap: 12px;
        }

        :host:after {
            display: block;
            position: absolute;
            bottom: 0;
            content: "";
            width: 100%;
            height: 100px;
            background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1));
        }

        .item {
            flex: 1 1 auto;
            object-fit: cover;
            object-position: center;
            max-height: 200px;
        }

        @media only screen and (min-width: 768px) {
            :host {
                max-height: 1200px;
            }

            .item {
                max-width: 300px;
            }
        }
    </style>
    <div id="wrapper">
    </div>
</template>