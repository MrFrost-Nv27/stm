<template id="image-card">
    <style>
        :host {
            position: relative;
            display: block;
            box-sizing: border-box;
        }

        #canvas {
            position: relative;
            overflow: hidden;
            background-color: rgba(0, 0, 0, 0.5);
            box-shadow: rgba(0, 0, 0, 0.6) 0px 0px 5px;
            width: 100%;
            height: 300px;
            margin: 0 auto;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            cursor: pointer;
            border-radius: 25px;
        }

        #canvas:hover {
            transform: scale(1.05);
        }

        #canvas:active {
            transform: scale(0.95);
        }

        .title {
            position: absolute;
            display: flex;
            justify-content: center;
            align-items: center;
            top: 0;
            left: 0;
            background-color: white;
            width: 100%;
            height: 30px;
        }

        .title p {
            margin: 0;
            padding: 0;
            font-size: 1rem;
            text-align: center;
        }

        #overlay {
            display: none;
            opacity: 0;
            background-color: rgba(0, 0, 0, 0.8);
            position: fixed;
            width: 100vw;
            height: 100vh;
            z-index: 999;
            top: 0;
            left: 0;
        }

        #modal {
            display: none;
            opacity: 0;
            position: fixed;
            z-index: 1000;
            background-color: white;
            width: 90%;
            height: 90%;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            border-radius: 25px;
            padding: 10px;
        }

        .modal-scroll {
            width: 100%;
            height: 500px;
            overflow: auto;
        }

        h2 {
            text-align: center;
        }

        p {
            text-align: justify;
            text-justify: inter-word;
        }

        .modal-close {
            position: absolute;
            display: inline;
            width: 40px;
            height: 40px;
            text-align: center;
            line-height: 40px;
            top: -15px;
            left: 50%;
            transform: translate(-50%, 0);
            background-color: white;
            border-radius: 50%;
            cursor: pointer;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 0px 5px;
            font-size: 1.5rem;
        }

        #canvas,
        #overlay,
        #modal {
            transition: all ease-in-out 0.3s;
        }

        .modal-close:hover {
            transform: translate(-50%, 0) scale(1.05);
        }

        .modal-close:active {
            transform: translate(-50%, 0) scale(0.95);
        }

        @media only screen and (min-width: 768px) {
            #modal {
                padding: 20px;
            }
        }
    </style>
    <div id="canvas">
        <div class="title">
            <p slot="title">Title</p>
        </div>
    </div>
    <div id="overlay"></div>
    <div id="modal">
        <div class="modal-close">x</div>
        <div slot="modal-content" class="modal-content">
            <h2 slot="modal-title" class="modal-title">Judul Modal</h2>
            <div class="modal-scroll">
                <p slot="modal-text" class="modal-text">Isi Modal</p>
            </div>
        </div>
    </div>
</template>