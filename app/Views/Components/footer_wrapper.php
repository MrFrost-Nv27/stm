<template id="footer-wrapper">
    <style>
    :host {
        display: block;
        width: 100%;
        padding-bottom: 16px;
    }

    #wrapper {}

    #title {
        position: relative;
        margin-bottom: 15px;
        padding-bottom: 10px;
        font-size: 1.2rem;
        font-weight: bold;
        text-align: center;
    }

    #title::before,
    #title::after {
        position: absolute;
        content: "";
        height: 5px;
        border-radius: 50px;
        bottom: 0;
    }

    #title::before {
        width: 100%;
        max-width: 400px;
        left: 50%;
        transform: translate(-50%, 0);
        background-color: var(--primary-dark);
        box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 4px;
    }

    #title::after {
        width: 60px;
        left: 50%;
        transform: translate(-50%, 0);
        background-color: #f2f2f2;
    }

    #content {
        position: relative;
        padding: 0 20px;
        text-align: center;
    }

    .name {
        text-transform: uppercase;
        font-size: 1.5rem;
        font-weight: bold;
        margin-bottom: 16px;
    }

    .description {
        font-size: small;
    }

    .logo {
        width: 100%;
        max-width: 150px;
        margin-bottom: 20px;
    }

    .map {
        position: relative;
        left: -10px;
        width: 100%;
        height: 250px;
        background-color: white;
        padding: 10px;
        overflow: hidden;
    }

    .mapouter {
        position: relative;
        text-align: right;
        width: 100%;
        height: 100%;
    }

    .gmap_canvas {
        overflow: hidden;
        background: none !important;
        width: 100%;
        height: 100%;
        box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 4px;
    }

    .gmap_iframe {
        height: 100% !important;
    }

    blank-data {
        color: black;
    }

    @media only screen and (min-width: 768px) {

        /* For desktop: */
        #title {
            text-align: start;
        }

        #title::before {
            max-width: 100%;
        }

        #title::after {
            left: 0;
            transform: none;
        }
    }
    </style>
    <div id="wrapper">
        <div id="title">
            Judul
        </div>
        <div id="content">
        </div>
</template>