<template id="blank-data">
    <style>
        :host {
            padding: 10px;
            background-color: white;
            display: block;
            border-radius: 5px;
            box-shadow: rgba(0, 0, 0, 0.5) 0px 0px 4px;
            max-width: 600px;
            margin: 0 auto;
        }

        p {
            padding: 0;
            margin: 0;
            font-size: .8rem;
            text-align: justify;
        }
    </style>
    <p slot="message" id="message">Pesan</p>
</template>