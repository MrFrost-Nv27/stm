<template id="tentang-box">
    <style>
        :host {
            display: block;
            width: 100%;
        }

        .title {
            position: relative;
            font-weight: bold;
            font-size: 1.25rem;
            background-color: white;
            margin-bottom: 30px;
            padding: 5px 10px;
            border-top-right-radius: 25px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 0px 5px;
        }

        .title::before {
            position: absolute;
            bottom: -8px;
            left: 0px;
            content: "";
            background-color: var(--primary);
            width: 45px;
            height: 8px;
            border-bottom-left-radius: 50px;
            border-bottom-right-radius: 50px;
            border-top-right-radius: 50px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 2px 5px;
        }

        .title::after {
            position: absolute;
            bottom: -8px;
            left: 50px;
            content: "";
            background-color: var(--primary);
            width: calc(100% - 50px);
            height: 8px;
            border-bottom-right-radius: 50px;
            border-bottom-left-radius: 50px;
            border-top-left-radius: 50px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 2px 5px;
        }

        .content {
            padding: 0;
        }

        p {
            text-align: justify;
            text-justify: inter-word;
        }

        @media only screen and (min-width: 992px) {
            .content {
                padding: 0 16px;
            }
        }
    </style>
    <div id="box">
        <div class="title">Title</div>
        <div class="content"></div>
    </div>
</template>