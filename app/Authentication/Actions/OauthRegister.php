<?php

declare (strict_types = 1);

namespace App\Authentication\Actions;

use CodeIgniter\Exceptions\PageNotFoundException;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\Shield\Authentication\Actions\ActionInterface;
use CodeIgniter\Shield\Authentication\Authenticators\Session;
use CodeIgniter\Shield\Authentication\Passwords;
use CodeIgniter\Shield\Config\AuthSession;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\Shield\Entities\UserIdentity;
use CodeIgniter\Shield\Exceptions\LogicException;
use CodeIgniter\Shield\Exceptions\RuntimeException;
use CodeIgniter\Shield\Exceptions\ValidationException;
use CodeIgniter\Shield\Models\UserIdentityModel;
use CodeIgniter\Shield\Traits\Viewable;
use Config\Auth;
use Config\Services;
use stdClass;

class OauthRegister implements ActionInterface
{
    use Viewable;

    private string $type = 'email_oauth';

    /**
     * Shows the initial screen to the user telling them
     * that an email was just sent to them with a link
     * to confirm their email address.
     */
    public function show(): string
    {
        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        $user = $authenticator->getPendingUser();
        if ($user === null) {
            throw new RuntimeException('Cannot get the pending login User.');
        }

        $userEmail = $user->email;
        if ($userEmail === null) {
            throw new LogicException(
                'Oauth Registration needs user email address. user_id: ' . $user->id
            );
        }

        $url = new stdClass;
        $url->api = 'http://api.stm.project/';
        $url->ppdb = 'http://ppdb.stm.project/';

        // Display the info page
        return $this->view('Pages/Auth/register_oauth', ["url" => $url, "user" => $user]);
    }

    /**
     * This method is unused.
     *
     * @return Response|string
     */
    public function handle(IncomingRequest $request)
    {
        throw new PageNotFoundException();
    }

    /**
     * Verifies the email address and code matches an
     * identity we have for that user.
     *
     * @return RedirectResponse|string
     */
    public function verify(IncomingRequest $request)
    {
        helper('setting');
        $url = new stdClass;
        $url->api = 'http://api.stm.project/';
        $url->ppdb = 'http://ppdb.stm.project/';
        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        $user = $authenticator->getPendingUser();
        if ($user === null) {
            throw new RuntimeException('Cannot get the pending login User.');
        }

        $validation = Services::validation();
        $validation->setRules($this->getValidationRules());

        if (!$validation->run($request->getPost(), null, config('Auth')->DBGroup)) {
            return redirect()->back()->withInput()->with('errors', $validation->getErrors());
        }

        $users = model(config('Auth')->userProvider);
        $userIdentityModel = model(UserIdentityModel::class);
        // Save the user
        $allowedPostFields = array_keys($this->getValidationRules());
        $user->fill($request->getPost($allowedPostFields));

        try {
            $users->save($user);
            // On success - remove the identity
            $userIdentityModel->deleteIdentitiesByType($user, $this->type);

            // Clean up our session
            $this->removeSessionKey('auth_action');
            $this->removeSessionKey('auth_action_message');

            $authenticator->completeLogin($user);
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()->with('errors', $users->errors());
        }

        $user = $authenticator->getUser();
        // Set the user active now
        $user->activate();

        // Success!
        return redirect()->to(config(Auth::class)->registerRedirect())
            ->with('message', lang('Auth.registerSuccess'));
    }

    /**
     * Creates an identity for the action of the user.
     *
     * @return string secret
     */
    public function createIdentity(User $user): string
    {
        /** @var UserIdentityModel $identityModel */
        $identityModel = model(UserIdentityModel::class);

        // Delete any previous identities for action
        $identityModel->deleteIdentitiesByType($user, $this->type);
        $identity = new UserIdentity([
            'user_id' => $user->id,
            'type' => $this->type,
            'name' => 'oauth',
            'secret' => gapi('oauth')->getStore()->refresh_token,
            'extra' => 'Silahkan selesaikan pengisian data anda untuk melanjutkan',
        ]);
        $identityModel->save($identity);
        return $identity->secret;
    }

    /**
     * Returns an identity for the action of the user.
     */
    private function getIdentity(User $user):  ? UserIdentity
    {
        /** @var UserIdentityModel $identityModel */
        $identityModel = model(UserIdentityModel::class);

        return $identityModel->getIdentityByType(
            $user,
            $this->type
        );
    }

    /**
     * Returns the string type of the action class.
     */
    public function getType() : string
    {
        return $this->type;
    }

    protected function getValidationRules(): array
    {
        $registrationUsernameRules = array_merge(
            config(AuthSession::class)->usernameValidationRules,
            [sprintf('is_unique[%s.username]', config(Auth::class)->tables['users'])]
        );

        return setting('Validation.registration') ?? [
            'username' => [
                'label' => 'Auth.username',
                'rules' => $registrationUsernameRules,
            ],
            'password' => [
                'label' => 'Auth.password',
                'rules' => 'required|' . Passwords::getMaxLengthRule() . '|strong_password[]',
                'errors' => [
                    'max_byte' => 'Auth.errorPasswordTooLongBytes',
                ],
            ],
            'password_confirm' => [
                'label' => 'Auth.passwordConfirm',
                'rules' => 'required|matches[password]',
            ],
        ];
    }

    private function removeSessionKey(string $key): void
    {
        helper('setting');
        $sessionUserInfo = $this->getSessionUserInfo();
        unset($sessionUserInfo[$key]);
        session()->set(setting('Auth.sessionConfig')['field'], $sessionUserInfo);
    }

    /**
     * Gets User Info in Session
     */
    private function getSessionUserInfo(): array
    {
        helper('setting');
        return session(setting('Auth.sessionConfig')['field']) ?? [];
    }
}
