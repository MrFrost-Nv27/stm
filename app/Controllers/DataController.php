<?php

namespace App\Controllers;

use App\Libraries\UrlGenerator;
use CodeIgniter\Config\Factories;
use CodeIgniter\Shield\Exceptions\InvalidArgumentException;

trait DataController
{
    protected array $pageData = [];
    protected array $dataset = [];

    protected string $title = 'Judul';
    protected string $modelClass;
    protected string $menuID;
    protected string $viewFolder;

    private function setInitData(): void
    {
        $this->dataset['url'] = Factories::libraries(UrlGenerator::class);
        if ($this->dataset) {
            foreach ($this->dataset as $key => $value) {
                $this->pageData[$key] = $value;
            }
        }

        $this->setTitle($this->title);
    }

    protected function setTitle(String $title): void
    {
        $this->pageData['title'] = $title;
    }

    protected function addData($input, ...$inputExt): void
    {
        if (is_string($input)) {
            if ($inputExt === []) {
                throw new InvalidArgumentException('Data harus berupa pasangan string key dan dynamic value');
            }
            $this->pageData[$input] = $inputExt[0];
        } elseif (is_array($input) && $input !== null && !array_is_list($input)) {
            foreach ($input as $key => $value) {
                $this->pageData[$key] = $value;
            }
        } else {
            throw new InvalidArgumentException('Data harus berupa key value pair atau array asosiatif');
        }
    }

    protected function addMenu(array $option = [])
    {
        $result = [
            'id' => $option['id'] ?? random_string(),
            'icon' => $option['icon'] ?? 'fa-solid fa-grid',
            'label' => $option['label'] ?? 'Label',
            'link' => $option['link'] ?? '#',
        ];

        $this->pageData['menu'][] = $result;
        return $this;
    }

    private function setInitMenu(): void
    {
        $this->addMenu([
            'id' => 'dashboard',
            'label' => 'Dashboard',
            'icon' => 'fa-solid fa-table-columns',
            'link' => url_to('panel'),
        ])->addMenu([
            'id' => 'meta',
            'label' => 'Edit Meta',
            'icon' => 'fa-solid fa-circle-info',
            'link' => url_to('panel-meta'),
        ]);
    }
}
