<?php

namespace App\Controllers\Backend;

use App\Controllers\BaseController;
use App\Libraries\UrlGenerator;
use CodeIgniter\Config\Factories;
use CodeIgniter\Shield\Authentication\Authenticators\Session;
use CodeIgniter\Shield\Authentication\JWTManager;

class LoginController extends BaseController
{
    public function attempt()
    {
        if (auth('session')->loggedIn()) {
            return redirect()->back();
        }
        $jwt = $this->request->getVar('jwt');
        if (!$jwt) {
            return redirect()->to(Factories::libraries(UrlGenerator::class)->auth . 'login/jwt?url=' . base64_encode(url_to('login')));
        }

        $targetjwt = session()->get('targetjwt');
        /** @var JWTManager $manager */
        $payload = service('jwtmanager')->parse($jwt);
        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        if (time() > $payload->exp) {
            return redirect()->to(Factories::libraries(UrlGenerator::class)->auth . 'login/jwt?url=' . base64_encode(url_to('login')));
        }
        $authenticator->remember(true)->loginById($payload->sub);
        session()->remove('targetjwt');

        return redirect()->to($targetjwt ?? base_url())->withCookies();
    }

    public function destroy()
    {
        auth()->logout();
        return redirect()->to(base_url())->with('message', lang('Auth.successLogout'));
    }
}
