<?php

namespace App\Controllers;

use CodeIgniter\Files\Exceptions\FileException;
use CodeIgniter\Files\File;

class SourceController extends BaseController
{
    public function script(...$path)
    {
        $filepath = ROOTPATH . "src" . DIRECTORY_SEPARATOR  . 'script' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $path);

        if (!is_file($filepath)) {
            throw new FileException('Filepath tidak valid');
        }
        $file = new File($filepath);

        header('Content-Type: text/javascript');
        echo file_get_contents($filepath);
        die;
    }

    public function style(...$path)
    {
        $filepath = ROOTPATH . "src" . DIRECTORY_SEPARATOR  . 'style' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $path);

        if (!is_file($filepath)) {
            throw new FileException('Filepath tidak valid');
        }
        $file = new File($filepath);

        header('Content-Type: text/css');
        echo file_get_contents($filepath);
        die;
    }

    public function storageDocument(...$path)
    {
        $filepath = WRITEPATH . "storage" . DIRECTORY_SEPARATOR  . 'Documents' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $path);

        if (!is_file($filepath)) {
            throw new FileException('Filepath tidak valid');
        }
        $file = new File($filepath);

        header('Content-Type: text/css');
        echo file_get_contents($filepath);
        die;
    }

    public function storageImage(...$path)
    {
        $filepath = WRITEPATH . "storage" . DIRECTORY_SEPARATOR  . 'Images' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $path);

        if (!is_file($filepath)) {
            throw new FileException('Filepath tidak valid');
        }
        $file = new File($filepath);

        header('Content-Type: image');
        echo file_get_contents($filepath);
        die;
    }
}
