<?php

namespace App\Controllers\Panel;

use App\Controllers\BaseDashboard;

class PanelView extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Dashboard';

    public function index()
    {
        $this->addData('pagePanel', 'dashboard');
        return view('Pages/Panel/index', $this->pageData);
    }

    public function meta()
    {
        $this->setTitle('Edit Info Meta');
        $this->addData('pagePanel', 'meta');
        return view('Pages/Panel/meta', $this->pageData);
    }
}
