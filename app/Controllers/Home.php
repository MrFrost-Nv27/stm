<?php

namespace App\Controllers;

use App\Libraries\UrlGenerator;
use CodeIgniter\Config\Factories;

class Home extends BaseController
{
    public function index()
    {
        return view('Pages/landing', [
            'url' => Factories::libraries(UrlGenerator::class),
        ]);
    }
}
