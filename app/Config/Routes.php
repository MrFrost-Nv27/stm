<?php

namespace Config;

use App\Controllers\Backend\LoginController;
use App\Controllers\Panel\PanelView;
use App\Controllers\SourceController;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('/script/(:any)', [SourceController::class, 'script'], );
$routes->get('/style/(:any)', [SourceController::class, 'style'], );
$routes->get('/store/document/(:any)', [SourceController::class, 'storageDocument'], );
$routes->get('/store/image/(:any)', [SourceController::class, 'storageImage'], );

$routes->get('/login', [LoginController::class, 'attempt'], ['as' => 'login']);
$routes->get('/logout', [LoginController::class, 'destroy'], ['as' => 'logout']);

$routes->group('panel', ['filter' => 'client:admin.access'], static function ($routes) {
    $routes->get('', [PanelView::class, 'index'], ['as' => 'panel']);
    $routes->get('meta', [PanelView::class, 'meta'], ['as' => 'panel-meta']);
});
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
