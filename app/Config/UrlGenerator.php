<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class UrlGenerator extends BaseConfig
{
    public string $domain = 'http://stm.project/';
    public array $sub = [
        'api',
        'ppdb',
        'auth',
        'blog',
    ];
}
