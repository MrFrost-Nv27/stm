<?php

namespace App\Libraries;

use App\Models\ModelMeta;
use EmptyIterator;
use JsonSerializable;

class MetaGenerator implements JsonSerializable
{
    public string $model = ModelMeta::class;
    public array $values = [];
    public array $query = [];

    public function __construct()
    {
        $this->query = model($this->model)->findAll();
        if ($this->query) {
            $this->initialization();
        }
    }

    protected function initialization()
    {
        foreach ($this->query as $query) {
            $this->values[$query->key] = new MetaObject(
                $query->format,
                $query->type,
                $query->addon
            );
        }
    }

    public function __get($property)
    {
        if ($this->values[$property]) {
            return $this->values[$property];
        }
    }

    public function jsonSerialize(): mixed
    {
        $data = [];
        if ($this->values) {
            foreach ($this->values as $key => $value) {
                $data[$key] = $value->value;
            }
            return $data;
        }
        return new EmptyIterator;
    }
}
