<?php

namespace App\Libraries;

use ArrayIterator;
use EmptyIterator;
use Exception;
use IteratorAggregate;
use Traversable;

class MetaObject implements IteratorAggregate
{
    public function __construct(
        public $value,
        public $type,
        public $addon,
    ) {
    }

    public function __toString()
    {
        if (is_array($this->value)) {
            throw new Exception("Nilai dari meta ini adalah array");
        }
        return $this->value;
    }

    public function getIterator(): Traversable
    {
        if (is_array($this->value)) {
            return new ArrayIterator($this->value);
        }
        return new EmptyIterator;
    }
}