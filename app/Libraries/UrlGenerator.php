<?php

namespace App\Libraries;

use Config\UrlGenerator as Config;

class UrlGenerator
{
    public Config $config;
    public function __construct()
    {
        $this->config = config('UrlGenerator');
    }

    public function __get($prop): string | array
    {
        if ($prop === 'all') {
            $result = [];
            $result['main'] = $this->config->domain;
            foreach ($this->config->sub as $sub) {
                $result[$sub] = preg_replace('/(.*)\/\/(.*)/', "$1//{$sub}.$2", $this->config->domain);
            }
            return $result;
        }
        return in_array($prop, $this->config->sub) ? preg_replace('/(.*)\/\/(.*)/', "$1//{$prop}.$2", $this->config->domain) : null;
    }

    public function __toString()
    {
        return $this->config->domain;
    }
}
