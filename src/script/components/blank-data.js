class BlankData extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("blank-data");
    this.shadow.append(template.content.cloneNode(true));
  }

  /**
   * @param {string} msg
   */
  set pesan(msg) {
    this.message = msg;
    this.updateMessage();
  }

  static get observedAttributes() {
    return ["pesan"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "pesan") {
      this.message = newValue;
      this.updateMessage();
    }
  }

  updateMessage() {
    const mEl = this.shadow.getElementById("message");
    mEl.textContent = this.message;
  }
}

customElements.define("blank-data", BlankData);
