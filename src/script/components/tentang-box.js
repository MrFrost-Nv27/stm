import "./blank-data.js";

class TentangBox extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    const template = document.getElementById("tentang-box");
    this.shadowRoot.append(template.content.cloneNode(true));
    const content = this.shadowRoot.querySelector(".content");

    this.emptyContent();
    this.blank = document.createElement("blank-data");
    this.blank.pesan = "Belum ada data yang dapat ditampilkan";
    content.append(this.blank);
  }

  set fillContent(html) {
    this.updateContent(html);
  }

  static get observedAttributes() {
    return ["title", "pesan"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "title") {
      this.updateTitle(newValue);
    }
    if (name === "pesan") {
      this.blank.pesan = newValue;
    }
  }

  emptyContent() {
    const content = this.shadowRoot.querySelector(".content");
    while (content.hasChildNodes()) {
      content.removeChild(content.firstChild);
    }
  }

  updateTitle(t = "Title") {
    this.shadowRoot.querySelector(".title").textContent = t;
  }

  updateContent(html) {
    this.shadowRoot.querySelector(".content").innerHTML = html;
  }

  fillAppend(element = new HTMLElement()) {
    const content = this.shadowRoot.querySelector(".content");
    content.innerHTML = "";
    content.append(element);
  }
}

customElements.define("tentang-box", TentangBox);
