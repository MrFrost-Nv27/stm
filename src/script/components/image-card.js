class ImageCard extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("image-card");
    this.shadow.append(template.content.cloneNode(true));
    const canvas = this.shadowRoot.querySelector("#canvas");
    const overlay = this.shadowRoot.querySelector("#overlay");
    const modalClose = this.shadowRoot.querySelector(".modal-close");
    canvas.style.backgroundImage = `url("https://picsum.photos/400/600")`;

    canvas.addEventListener("click", this.cardClicked, false);
    overlay.addEventListener("click", (e) => this.closeModal());
    modalClose.addEventListener("click", (e) => this.closeModal());
  }

  /**
   * @param {string} url
   */
  set setImage(url) {
    this._imageUrl = url;
    this.updateImage();
  }

  /**
   * @param {string} html
   */
  set setModalContent(html) {
    this.shadowRoot.querySelector(".modal-content").innerHTML = html;
  }

  static get observedAttributes() {
    return ["width", "height", "image", "title", "title-text", "title-position", "modal-title", "modal-text"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    const canvas = this.shadowRoot.querySelector("#canvas");
    const title = this.shadowRoot.querySelector(".title");
    const titleText = this.shadowRoot.querySelector(".title p");
    if (name === "image") {
      this.setImage = newValue;
    }
    if (name === "width") {
      canvas.style.maxWidth = newValue;
    }
    if (name === "height") {
      canvas.style.height = newValue;
    }
    if (name === "title") {
      title.style.display = newValue.toLowerCase() === "true" ? "flex" : "none";
    }
    if (name === "title-text") {
      titleText.textContent = newValue;
    }
    if (name === "title-position") {
      if (newValue === "top") {
        title.style.top = "0";
        title.style.bottom = "auto";
      }
      if (newValue === "bottom") {
        title.style.top = "auto";
        title.style.bottom = "0";
      }
    }
    if (name === "modal-title") {
      this.shadowRoot.querySelector(`.${name}`).textContent = newValue;
    }
    if (name === "modal-text") {
      this.shadowRoot.querySelector(`.${name}`).innerHTML = newValue;
    }
  }

  updateImage() {
    this.shadowRoot.querySelector("#canvas").style.backgroundImage = `url("${this._imageUrl}")`;
  }

  closeModal() {
    const showing = new Promise((resolve, reject) => {
      this.shadowRoot.querySelector("#overlay").style.opacity = "0";
      this.shadowRoot.querySelector("#modal").style.opacity = "0";
      setTimeout(() => {
        resolve(true);
      }, 300);
    });
    showing.then(() => {
      this.shadowRoot.querySelector("#overlay").style.display = "none";
      this.shadowRoot.querySelector("#modal").style.display = "none";
    });
  }

  cardClicked = (e) => {
    const showing = new Promise((resolve, reject) => {
      this.shadowRoot.querySelector("#overlay").style.display = "block";
      this.shadowRoot.querySelector("#modal").style.display = "block";
      setTimeout(() => {
        resolve(true);
      }, 100);
    });
    showing.then(() => {
      this.shadowRoot.querySelector("#overlay").style.opacity = "1";
      this.shadowRoot.querySelector("#modal").style.opacity = "1";
    });
  };

  removeCardClicked() {
    this.shadowRoot.querySelector("#canvas").removeEventListener("click", this.cardClicked, false);
  }

  setCardClicked(f = () => {}) {
    this.removeCardClicked();
    this.cardClicked = f;
    this.shadowRoot.querySelector("#canvas").addEventListener("click", this.cardClicked, false);
  }
}

customElements.define("image-card", ImageCard);
