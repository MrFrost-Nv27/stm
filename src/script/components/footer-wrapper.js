import "./blank-data.js";

class FooterWrapper extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("footer-wrapper");
    this.shadow.append(template.content.cloneNode(true));

    this.blank = document.createElement("blank-data");
    this.blank.pesan = "Belum ada data yang dapat ditampilkan";
    this.shadow.querySelector("#content").append(this.blank);
  }

  static get observedAttributes() {
    return ["title", "no-title"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "title") {
      this.shadowRoot.querySelector("#title").textContent = newValue;
    }
    if (name === "no-title") {
      this.shadowRoot.querySelector("#title").style.display = newValue.toLowerCase() === "true" ? "none" : "block";
    }
  }

  addName(name = "Name") {
    this.blank.remove();
    const el = document.createElement("div");
    el.classList.add("name");
    el.textContent = name;
    this.shadowRoot.querySelector("#content").append(el);
  }

  addLogo(src = "https://picsum.photos/300/300") {
    this.blank.remove();
    const el = document.createElement("img");
    el.classList.add("logo");
    el.alt = "Logo";
    el.src = src;
    this.shadowRoot.querySelector("#content").append(el);
  }

  addDescription(description = "Description") {
    this.blank.remove();
    const el = document.createElement("div");
    el.classList.add("description");
    el.textContent = description;
    this.shadowRoot.querySelector("#content").append(el);
  }

  addMap(mapHtml = "") {
    this.blank.remove();
    const el = document.createElement("div");
    el.classList.add("map");
    el.innerHTML = mapHtml;
    this.shadowRoot.querySelector("#content").append(el);
  }

  fillContent(html = "", emptyContent = true) {
    this.blank.remove();
    if (emptyContent) {
      this.emptyContent();
    }
    this.shadowRoot.querySelector("#content").innerHTML = html;
  }

  fillAppend(el = new HTMLElement(), emptyContent = true) {
    this.blank.remove();
    if (emptyContent) {
      this.emptyContent();
    }
    this.shadowRoot.querySelector("#content").prepend(el);
  }

  emptyContent() {
    this.shadowRoot.querySelector("#content").innerHTML = "";
  }
}

customElements.define("footer-wrapper", FooterWrapper);
