class SocialMedia extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("social-media");
    this.shadow.append(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return ["link", "username", "icon"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "link") {
      this.shadowRoot.querySelector("a").href = newValue;
    }
    if (name === "username") {
    }
    if (name === "icon") {
    }
  }

  addIcon(svg = new SVGElement()) {
    this.shadowRoot.querySelector("a").innerHTML = svg;
  }
}

customElements.define("social-media", SocialMedia);
