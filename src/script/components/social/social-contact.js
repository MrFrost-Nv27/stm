class SocialContact extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("social-contact");
    this.shadow.append(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return ["link", "username", "icon"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "link") {
      this.shadowRoot.querySelector("a").href = newValue;
    }
    if (name === "username") {
      this.shadowRoot.querySelector("#username").textContent = newValue;
    }
    if (name === "icon") {
    }
  }

  addIcon(svg = new SVGElement()) {
    this.shadowRoot.querySelector("#icon").innerHTML = svg;
  }
}

customElements.define("social-contact", SocialContact);
