class GalleryWrapper extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("gallery-wrapper");
    this.shadow.append(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return [];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "width") {
    }
  }

  addItem(url = "https://picsum.photos/200/300") {
    const img = document.createElement("img");
    img.classList.add("item");
    img.src = url;
    this.shadowRoot.querySelector("#wrapper").append(img);
  }

  addItems(urls = []) {
    urls.forEach((url) => {
      this.addItem(url);
    });
  }
}

customElements.define("gallery-wrapper", GalleryWrapper);
