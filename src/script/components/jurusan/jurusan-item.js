class JurusanItem extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  get isOdd() {
    return this._isOdd;
  }

  /**
   * @param {boolean} val
   */
  set isOdd(val) {
    this._isOdd = val;
    let template;
    if (val) {
      template = document.getElementById("jurusan-item-odd");
    } else {
      template = document.getElementById("jurusan-item-even");
    }
    this.shadowRoot.append(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return ["odd"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "odd") {
    }
  }

  parseOptions(options = new Map()) {
    this.shadowRoot.querySelector("#image").style.backgroundImage = `url(${options.get("cover")})`;
    this.shadowRoot.querySelector("#title").textContent = options.get("nama");
    this.shadowRoot.querySelector("#desc").textContent = options.get("deskripsi");
  }
}

customElements.define("jurusan-item", JurusanItem);
