import "../blank-data.js";
import "./jurusan-item.js";

class JurusanList extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const template = document.getElementById("jurusan-list");
    this.blankEl = document.createElement("blank-data");
    this.blankEl.pesan = "Data Jurusan Belum Ditambahkan";
    this.blankEl.setAttribute("data-aos", "zoom-in");
    this.append(template.content.cloneNode(true));
    this.append(this.blankEl);
  }

  static get observedAttributes() {
    return [];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "width") {
    }
  }

  addItem(newOpt = {} | null, isOdd = true) {
    const options = new Map([
      ["kode", "Belum ada kode"],
      ["nama", "Belum ada nama"],
      ["deskripsi", "Belum ada deskripsi"],
      ["cover", "https://picsum.photos/400/600"],
    ]);

    if (newOpt !== null) {
      Object.entries(newOpt).forEach((newOp) => {
        const [key, value] = newOp;
        options.set(key, value);
      });
    }

    this.blankEl.remove();
    const item = document.createElement("jurusan-item");
    item.isOdd = isOdd;
    item.setAttribute("data-aos", isOdd ? "fade-right" : "fade-left");
    item.parseOptions(options);
    this.append(item);
  }

  addItems(jurusan = []) {
    jurusan.forEach((j, i) => {
      this.addItem(j, !!((i + 1) % 2));
    });
  }
}

customElements.define("jurusan-list", JurusanList);
