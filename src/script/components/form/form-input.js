class FormInput extends HTMLElement {
  constructor() {
    super();
    this._formInput = document.createElement("input");
    this._formLabel = document.createElement("label");
    this._formLabelText = document.createElement("div");
    this._formLabel.classList = "label";
    this._formLabelText.classList = "text";

    this._formInput.classList = "material-input";
    this._formLabel.style.top = "0%";

    this._initValue = "";
  }

  connectedCallback() {
    this.initComponent();
  }

  render(newItem = {}) {
    const baseOption = new Map([
      ["type", "text"],
      ["name", "input_name"],
      ["initValue", ""],
      ["autocomplete", true],
      ["required", true],
      ["label", "Label Input"],
    ]);
    const renderOption = newItem !== null ? this.optionParse(baseOption, newItem) : baseOption;

    this._initValue = renderOption.get("initValue");

    this._formInput.type = renderOption.get("type");
    this._formInput.name = renderOption.get("name");
    if (renderOption.get("autocomplete")) this._formInput.autocomplete = renderOption.get("name");
    if (renderOption.get("initValue") !== "") this._formInput.setAttribute("value", this._initValue);
    this._formInput.required = renderOption.get("required");

    this._formLabelText.innerText = renderOption.get("label");
    this._formLabel.replaceChildren(this._formLabelText);

    this.replaceChildren(this._formInput, this._formLabel);
  }

  optionParse(option = new Map(), newOpt = {}) {
    if (option.size > 0 && newOpt !== null) {
      if (newOpt instanceof Map) {
        for (const [key, value] of newOpt) {
          option.set(key, value);
        }
      } else {
        Object.entries(newOpt).forEach((n) => {
          const [key, value] = n;
          option.set(key, value);
        });
      }
      return option;
    }
    return null;
  }

  initComponent() {
    this.style.display = "block";
    this.classList = "material input-container";
  }
}

customElements.define("form-input", FormInput);
