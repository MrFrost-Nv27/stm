import "../../components/form/form-input.js";

class FormWrapper extends HTMLElement {
  constructor() {
    super();
    this._formWrapper = document.createElement("form");
  }

  connectedCallback() {
    this.initComponent();
    this.replaceChildren(this._formWrapper);
  }

  addInput(newItem = {}, once = false) {
    const baseOption = new Map([
      ["type", "text"],
      ["name", "input_name"],
      ["initValue", ""],
      ["autocomplete", true],
      ["required", true],
      ["label", "Label Input"],
    ]);
    const option = newItem !== null ? this.optionParse(baseOption, newItem) : baseOption;

    let newInput;
    switch (option.get("type")) {
      case "text":
        newInput = document.createElement("form-input");
        newInput.render(option);
        break;
      default:
        newInput = document.createElement("form-input");
        newInput.render(option);
        break;
    }
    once ? this._formWrapper.replaceChildren(newInput) : this._formWrapper.append(newInput);
  }

  setOnSubmit(func) {
    if (typeof func !== "function") {
      throw "the func parameter must be type of function, " + typeof func + " cast";
    }
    this._formWrapper.onsubmit = (event) => {
      event.preventDefault();
      func.call(this, event);
    };
  }

  optionParse(option = new Map(), newOpt = {}) {
    if (option.size > 0 && newOpt !== null) {
      if (newOpt instanceof Map) {
        for (const [key, value] of newOpt) {
          option.set(key, value);
        }
      } else {
        Object.entries(newOpt).forEach((n) => {
          const [key, value] = n;
          option.set(key, value);
        });
      }
      return option;
    }
    return null;
  }

  initComponent() {
    this.style.display = "block";
    this.style.paddingTop = "1rem";
    this.classList = "";
  }

  resetFormValue() {
    this.querySelectorAll("input").forEach((el) => {
      el.value = el.parentElement._initValue;
    });
  }
}

customElements.define("form-wrapper", FormWrapper);
