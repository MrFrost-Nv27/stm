class PostItem extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("post-item");
    this.shadow.append(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return [];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "width") {
    }
  }

  parseOptions(options = new Map()) {
    this.shadowRoot.querySelector("#image").style.backgroundImage = `url(${options.get("img")})`;
    this.shadowRoot.querySelector("#title").textContent = options.get("title");
    this.shadowRoot.querySelector("#text").textContent = options.get("text");
  }
}

customElements.define("post-item", PostItem);
