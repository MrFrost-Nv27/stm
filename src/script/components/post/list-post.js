import "./post-item.js";
import "../blank-data.js";

class ListPost extends HTMLElement {
  constructor() {
    super();
    this.itemAdded = false;
    this.shadow = this.attachShadow({ mode: "open" });
    const template = document.getElementById("list-post");
    this.shadow.append(template.content.cloneNode(true));

    this.blank = document.createElement("blank-data");
    this.blank.pesan = "Belum ada postingan terbaru";
    this.shadow.querySelector(".list-post").append(this.blank);
  }

  static get observedAttributes() {
    return [];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "width") {
    }
  }

  addPostItem(newOpt = {} | null, clear = false) {
    const listPost = this.shadow.querySelector(".list-post");
    const newPost = document.createElement("post-item");
    if (!this.itemAdded) {
      listPost.innerHTML = "";
      this.shadowRoot.querySelector(".post-button").style.display = "block";
    }
    this.itemAdded = true;
    const options = new Map([
      ["img", "https://picsum.photos/600/400"],
      ["title", "Post Title"],
      ["text", "Post Text ......"],
    ]);

    if (newOpt !== null) {
      Object.entries(newOpt).forEach((newOp) => {
        const [key, value] = newOp;
        options.set(key, value);
      });
    }

    if (clear) {
      listPost.innerHTML = "";
    }

    newPost.parseOptions(options);
    listPost.append(newPost);
  }
}

customElements.define("list-post", ListPost);
