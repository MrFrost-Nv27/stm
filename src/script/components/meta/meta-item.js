import "../../components/form/form-wrapper.js";
class MetaItem extends HTMLElement {
  constructor() {
    super();
    this.cardWrapper = document.createElement("a");
    this.cardTitle = document.createElement("p");
    this.cardIcon = document.createElement("i");
    this.formWrapper = document.createElement("form-wrapper");
    this.formWrapper.setOnSubmit(async (event) => {
      const data = Object.fromEntries(new FormData(event.target));
      const [key, value] = Object.entries(data)[0];
      const item = await store.getTable("stm_meta").find(key);
      item.value = value;

      document.querySelector("body").classList.add("ajax-loading");
      this.classList.add("loading");
      document.querySelector(".popup-wrapper").classList.remove("open");

      const fetch = await store.getTable("stm_meta").save(item, (item) => {
        const sendData = {};
        sendData[item.key] = item.value;
        return sendData;
      });
      swalToast.fire({
        icon: fetch.isSuccess ? "success" : "warning",
        title: fetch.message,
      });

      document.querySelector("body").classList.remove("ajax-loading");
      this.classList.remove("loading");

      this.render(fetch.extra.serialize());
    });

    this.cardWrapper.classList = "meta-card meta-editor";
    this.cardTitle.classList = "meta-card-title";
    this.cardIcon.classList = "fa-solid fa-pen-to-square";
  }

  render(newItem = {}) {
    const item = new Map([
      ["key", ""],
      ["value", ""],
      ["label", ""],
      ["type", ""],
      ["addon", ""],
    ]);
    if (newItem !== null) {
      Object.entries(newItem).forEach((newOp) => {
        const [key, value] = newOp;
        item.set(key, value);
      });
    }

    this.setAttribute("data-testid", item.get("key"));
    this.itemData = item;
    switch (item.get("type")) {
      case "text":
        this.cardContent = document.createElement("p");
        this.cardContent.classList = "meta-card-desc";
        this.cardContent.innerText = item.get("value") ?? "";
        this.formWrapper.addInput(
          {
            name: this.itemData.get("key"),
            initValue: this.itemData.get("value"),
            label: this.itemData.get("label"),
          },
          true
        );
        break;
      case "number":
        this.cardContent = document.createElement("p");
        this.cardContent.classList = "meta-card-desc";
        this.cardContent.innerText = item.get("value") ?? "";
        this.formWrapper.addInput(
          {
            type: "number",
            name: this.itemData.get("key"),
            initValue: this.itemData.get("value"),
            label: this.itemData.get("label"),
          },
          true
        );
        break;
      case "html":
        this.cardContent = document.createElement("div");
        this.cardContent.classList = "meta-card-html";
        this.cardContent.innerHTML = item.get("value") ?? "";
        break;
      case "map":
        this.cardContent = document.createElement("p");
        this.cardContent.classList = "meta-card-desc text-center";
        this.cardContent.innerHTML = `<em>View on edit</em>`;
        break;
      default:
        this.cardContent = document.createElement("p");
        this.cardContent.classList = "meta-card-desc text-center";
        this.cardContent.innerHTML = `<em>View on edit</em>`;
        break;
    }
    this.cardTitle.innerText = item.get("label");
    this.cardWrapper.replaceChildren(this.cardTitle, this.cardContent, this.cardIcon);

    this.replaceChildren(this.cardWrapper);

    if (item.get("key").toString().length > 0 && item.get("value").toString().length > 0 && item.get("label").toString().length > 0) {
      this.onclick = this.handleOnClick;
    } else {
      this.remove();
      this.onclick = function () {};
    }
  }

  handleOnClick = (event) => {
    this.formWrapper.resetFormValue();
    popupTrigger.call(this, event, {
      title: this.itemData.get("label"),
      content: this.formWrapper,
    });
  };
}

customElements.define("meta-item", MetaItem);
