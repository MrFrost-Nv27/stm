import BaseData from "./base-data.js";

class DataUser extends BaseData {
  tableName = "meta";
  constructor() {
    super();
  }
  static async getAll() {
    return $.get("http://api.stm.project/v2/user");
  }
}

export default DataUser;
