import Url from "./url.js";
import ModelMeta from "./model/model-meta.js";

class Store {
  option = {};
  timestamp = {};
  models = {};

  constructor(options = {}) {
    this.setOption(options);
    this.updateTimestamp();
    this.models[ModelMeta.table] = new ModelMeta();
  }

  setOption(options = {}) {
    for (const key in options) {
      if (this.option.hasOwnProperty(key)) {
        this.option[key] = options[key];
      }
    }
  }

  getTable(table = "") {
    if (table == "" && !this.models.hasOwnProperty(table)) {
      return new Result({
        success: false,
        message: "table name cannot be blank",
      });
    }
    return this.models[table];
  }

  updateTimestamp() {
    $.get(Url.publicApi + "store").then((timestamp) => {
      this.timestamp = timestamp;
    });
  }
}

export default Store;
