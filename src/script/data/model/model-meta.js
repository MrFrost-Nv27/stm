import EntityMeta from "../entity/entity-meta.js";
import Model from "../model.js";
import Url from "../url.js";

class ModelMeta extends Model {
  static table = "stm_meta";

  constructor() {
    super({
      entity: EntityMeta,
      primary: "key",
      url: {
        create: null,
        read: Url.publicApi + "meta/all",
        update: Url.privateApi + "master/meta",
        delete: null,
      },
    });
  }
}

export default ModelMeta;
