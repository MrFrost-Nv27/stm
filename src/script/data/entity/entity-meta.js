import Entity from "../entity.js";

class EntityMeta extends Entity {
  static attributes = {
    key: "key",
    value: "value",
    type: "type",
    addon: "addon",
    label: "label",
  };
  static required = [this.attributes.key, this.attributes.value, this.attributes.type];

  constructor(data) {
    super(data);
  }

  getAttributes() {
    return EntityMeta.attributes;
  }

  getRequired() {
    return EntityMeta.required;
  }

  getPrimary() {
    return "key";
  }
}

export default EntityMeta;
