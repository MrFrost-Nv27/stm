class Result {
  result = {
    success: true,
    message: "",
    extra: {},
  };

  constructor(options = {}) {
    this.setOption(options);
  }

  setOption(options = {}) {
    for (const key in options) {
      if (this.result.hasOwnProperty(key)) {
        this.result[key] = options[key];
      }
    }
  }

  get isSuccess() {
    return this.result.success == true;
  }

  get message() {
    return this.result.message;
  }

  get extra() {
    return this.result.extra;
  }
}

export default Result;
