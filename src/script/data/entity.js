import Result from "./result.js";

class Entity {
  static attributes = {};
  static required = [];
  static handler = {
    get(target, property) {
      if (property in target) {
        if (typeof target[property] == "function") return target[property];
      }
      if ("get" + snakeToPascal(property) in target) {
        return target["get" + snakeToPascal(property)]();
      }
      return property in target.getAttributes() ? target.newValues[property] ?? target.values[property] ?? null : null;
    },
    set(target, property, value) {
      if (property in target.getAttributes()) {
        target.newValues[property] = value;
        return true;
      }
      return false;
    },
  };
  values = {};
  newValues = {};

  constructor(data = {}) {
    this.setValues(data);
  }

  getAttributes() {
    return Entity.attributes;
  }

  getRequired() {
    return Entity.required;
  }

  getPrimary() {
    return "id";
  }

  setValues(data = {}) {
    for (const key in data) {
      if (this.getAttributes().hasOwnProperty(key)) {
        this.values[key] = data[key];
      }
    }
  }

  ready() {
    this.getRequired().forEach((k) => {
      let requiredEmpty = [];
      if (this[k] == null) {
        requiredEmpty.push(k);
      }
      if (requiredEmpty.length > 0)
        return new Result({
          success: false,
          message: requiredEmpty.toString() + " is must have value",
        });
    });

    if (this[this.getPrimary()] == null) {
      return new Result({
        success: true,
        message: "item was ready to save",
        extra: "create",
      });
    } else {
      return new Result({
        success: true,
        message: "item was ready to save",
        extra: "update",
      });
    }
  }

  serialize() {
    const attr = this.getAttributes();
    const data = {};
    Object.keys(attr).forEach((k) => {
      data[attr[k]] = this[attr[k]];
    });
    return data;
  }
}

export default Entity;
