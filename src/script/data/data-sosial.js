import BaseData from "./base-data.js";
import Url from "./url.js";

class DataSosial extends BaseData {
  tableName = "jurusan";
  constructor() {
    super();
  }
  static async getAll() {
    return $.get(Url.publicApi + "sosial");
  }
}

export default DataSosial;
