class Url {
  static publicApi = `${UrlGenerator.api}v2/`;
  static privateApi = `${UrlGenerator.api}`;
}

export default Url;
