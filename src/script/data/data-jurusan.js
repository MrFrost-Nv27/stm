import BaseData from "./base-data.js";
import Url from "./url.js";

class DataJurusan extends BaseData {
  tableName = "jurusan";
  constructor() {
    super();
  }
  static async getAll() {
    return $.get(Url.publicApi + "jurusan");
  }
}

export default DataJurusan;
