import Result from "./result.js";

class Model {
  static table = "";
  option = {
    entity: null,
    primary: "id",
    url: {
      create: null,
      read: null,
      update: null,
      delete: null,
    },
  };
  data = null;
  hasFind = false;

  constructor(options = {}) {
    this.setOption(options);
  }

  setOption(options = {}) {
    for (const key in options) {
      if (this.option.hasOwnProperty(key)) {
        this.option[key] = options[key];
      }
    }
  }

  async save(item, preprocess = null) {
    const check = item.ready();
    if (check.isSuccess) {
      const saveUrl = this.option.url[check.extra];
      const dataItem = typeof preprocess == "function" ? preprocess(item.serialize()) : item.serialize();
      const fetch = await $.ajax({
        url: saveUrl,
        type: "post",
        dataType: "json",
        data: dataItem,
      })
        .then(async (d) => {
          const ex = await this[check.extra + "Handle"](item);
          return new Result({
            success: true,
            message: d.message,
            extra: ex ?? item,
          });
        })
        .catch(
          (r) =>
            new Result({
              success: false,
              message: r.statusText,
            })
        );
      return fetch;
    }
    return check;
  }

  async findAll() {
    if (this.option.url.read == null) {
      return new Result({
        success: false,
        message: "Read URL not define",
      });
    }
    if (this.data != null && !this.hasFind) {
      return this.data;
    }
    const fetch = await $.ajax({
      url: this.option.url.read,
      type: "get",
      dataType: "json",
      success: this.createItems,
    });
    this.hasFind = false;
    return fetch;
  }
  async find(value, only = null) {
    if (this.option.url.read == null) {
      return new Result({
        success: false,
        message: "Read URL not define",
      });
    }
    if (this.data != null && !this.hasFind) {
      const filterredData = this.data.filter((item) => item[this.option.primary] == value);
      if (filterredData.length) {
        return only == null ? filterredData[0] : filterredData[0][only] ?? null;
      } else {
        return null;
      }
    }
    const fetch = await $.ajax({
      url: this.option.url.read + `/${value}`,
      type: "get",
      dataType: "json",
      success: this.createItem,
    });
    this.hasFind = true;
    return only == null ? fetch : fetch[only] ?? null;
  }

  createItem = (data, force = true) => {
    let result = [];
    if (this.option.entity == null) {
      result.push(data);
    } else {
      const newEntity = new this.option.entity(data);
      const newItem = new Proxy(newEntity, this.option.entity.handler);
      result.push(newItem);
    }
    if (force) this.data = result;
    return result;
  };

  createItems = (data) => {
    let result = [];
    for (const i in data) {
      result.push(this.createItem(data[i], false)[0]);
    }
    this.data = result;
    return result;
  };

  createHandle(item) {}
  async updateHandle(item) {
    const allData = await this.findAll();
    const filterredData = allData.findIndex((choose) => choose[this.option.primary] == item[this.option.primary]);
    const newEntity = new this.option.entity(item.serialize());
    const updatedItem = new Proxy(newEntity, this.option.entity.handler);
    allData[filterredData] = updatedItem;
    return allData[filterredData];
  }
}

export default Model;
