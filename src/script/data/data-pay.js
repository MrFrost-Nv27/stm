import BaseData from "./base-data.js";
import Url from "./url.js";

class DataPay extends BaseData {
  tableName = "pembayaran";
  constructor() {
    super();
  }
  static async getAll() {
    return $.get(Url.publicApi + "pay");
  }
}

export default DataPay;
