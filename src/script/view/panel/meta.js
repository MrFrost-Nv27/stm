import "../../components/meta/meta-item.js";

const metaWrapper = $(".meta-card-wrapper");

const meta = async (data) => {
  console.log(`on Panel ${currentPanel}`);
  const metaRender = (dataRender = data.meta) => {
    metaWrapper.empty();
    $.each(dataRender, function (i, v) {
      if (v.type == "image" || v.type == "boolean") {
        return;
      }
      const newMeta = document.createElement("meta-item");
      newMeta.render(v);
      metaWrapper.append(newMeta);
    });
  };

  metaRender();
};

export default meta;
