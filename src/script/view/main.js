import landing from "./landing.js";
import "../components/tentang-box.js";
import "../components/image-card.js";
import "../components/post/list-post.js";
import "../components/gallery-wrapper.js";
import "../components/footer-wrapper.js";
import "../components/jurusan/jurusan-list.js";
import panel from "./panel.js";

const popup = $(".popup-wrapper");

const main = () => {
  $("body").on("click", ".btn-print", function (event) {
    $($(this).data("print")).prepend($(".kop-wrapper")).printThis();
  });

  $("body").on("click", ".popup-overlay", function (event) {
    popup.removeClass("open");
  });
  $("body").on("click", ".popup-close", function (event) {
    popup.removeClass("open");
  });

  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
  const tooltipList = [...tooltipTriggerList].map((tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl));

  switch (current) {
    case "index":
      landing();
      break;
    case "panel":
      panel();
      break;
    default:
      break;
  }
};

export default main;
