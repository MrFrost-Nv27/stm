import ModelMeta from "../data/model/model-meta.js";
import meta from "./panel/meta.js";

const topMenuList = $(".menu-list.menu-top ul");
const addMenu = (
  options = {
    id: "menuid",
    icon: "fa-solid fa-table-columns",
    label: "Label",
    link: "#!",
  }
) => {
  const menuItem = $(`<li data-menuid="${options.id}"><a href="${options.link}"><i class="${options.icon}"></i> ${options.label}</a></li>`);
  topMenuList.append(menuItem);
};

const panel = async () => {
  console.log(`init ${current} page.....`);
  AOS.init();

  if (Menus != null) {
    $.each(Menus, function (i, v) {
      addMenu(v);
    });
  }

  $(".preloader").slideUp();
  $(`.menu-list.menu-top li[data-menuid!="${currentPanel}"]`).removeClass("active");
  $(`.menu-list.menu-top li[data-menuid="${currentPanel}"]`).addClass("active");

  const logoLembaga2 = await store.getTable(ModelMeta.table).find("logo_lembaga_2", "value");
  const tahunWeb = await store.getTable(ModelMeta.table).find("tahun_web", "value");

  if (logoLembaga2) $(".logo").attr("src", logoLembaga2);

  $(".copyright").text(function () {
    const createdYear = tahunWeb ?? 2023;
    const now = new Date().getFullYear();
    return createdYear == now ? createdYear : `${createdYear} - ${now}`;
  });

  switch (currentPanel) {
    case "dashboard":
      break;
    case "meta":
      store
        .getTable(ModelMeta.table)
        .findAll()
        .then((metas) => {
          meta({
            meta: metas,
          });
        });
      break;
    default:
      break;
  }
};

export default panel;
