import beranda from "./landing/beranda.js";
import tentang from "./landing/tentang.js";
import jurusan from "./landing/jurusan.js";
import biaya from "./landing/biaya.js";
import footer from "./landing/footer.js";
import DataUser from "../data/data-user.js";
import ModelMeta from "../data/model/model-meta.js";
import EntityMeta from "../data/entity/entity-meta.js";

const landing = async () => {
  console.log(`init ${current} page.....`);
  AOS.init();

  $("#beranda").on("inview", function (event, isInView) {
    if (isInView) {
      const id = $(this).attr("id");
      changeActiveMenu(id);
    }
  });

  $("#tentang").on("inview", function (event, isInView) {
    if (isInView) {
      const id = $(this).attr("id");
      changeActiveMenu(id);
    }
  });

  $("#jurusan").on("inview", function (event, isInView) {
    if (isInView) {
      const id = $(this).attr("id");
      changeActiveMenu(id);
    }
  });

  $("#biaya").on("inview", function (event, isInView) {
    if (isInView) {
      const id = $(this).attr("id");
      changeActiveMenu(id);
    }
  });

  function changeActiveMenu(id) {
    $(".topbar-menu a").removeClass("active");
    $(`.topbar-menu a[href='#${id}']`).addClass("active");
  }

  store
    .getTable(ModelMeta.table)
    .findAll()
    .then((val) => {
      store
        .getTable(ModelMeta.table)
        .find("logo_lembaga")
        .then((data) => $(".topbar-logo img").attr("src", data.value));
      $(".preloader").slideUp();
      $(".placeholder").removeClass("placeholder");
      $(".topbar-logo img").attr("src", "");
      beranda(store.getTable(ModelMeta.table));
      tentang(store.getTable(ModelMeta.table));
      jurusan(store.getTable(ModelMeta.table));
      biaya(store.getTable(ModelMeta.table));
      footer(store.getTable(ModelMeta.table));
    });
};

export default landing;
