const beranda = async (data) => {
  if (data.find("nama_lembaga_short", "value")) $(".hero-welcome p:nth-child(2)").text(await data.find("nama_lembaga_short", "value"));
  if (data.find("daerah_lembaga", "value")) $(".hero-welcome p:nth-child(3)").text(await data.find("daerah_lembaga", "value"));
  if (data.find("hero_text", "value")) $(".hero-box .row div:nth-child(1)").text(await data.find("hero_text", "value"));

  var i = 0;
  const slideTime = 5000;

  async function changePicture() {
    const pictures = await data.find("hero_image", "value");
    const hero = $(".hero");
    if (pictures && pictures.length > 0) {
      hero.css("background-image", "url(" + pictures[i] + ")");

      if (i < pictures.length - 1) {
        i++;
      } else {
        i = 0;
      }
      setTimeout(changePicture, slideTime);
    }
  }

  changePicture();
};

export default beranda;
