import "../../components/image-card.js";
import "../../components/gallery-wrapper.js";

const tentang = async (data) => {
  const elDeskripsi = $("<tentang-box></tentang-box>").attr("title", "Deskripsi").attr("pesan", "Deskripsi Belum ditambahkan");
  const elVisi = $("<tentang-box></tentang-box>").attr("title", "Visi & Misi").attr("pesan", "Visi & Misi Belum ditambahkan");
  const elKepsek = $("<tentang-box></tentang-box>").attr("title", "Kepala Sekolah").attr("pesan", "Kepala Sekolah Belum ditambahkan");
  const elPost = $("<tentang-box></tentang-box>").attr("title", "Postingan Terbaru").attr("pesan", "Belum ada artikel yang diposting");
  const elGallery = $("<tentang-box></tentang-box>").attr("title", "Galeri").attr("pesan", "Gambar untuk galeri Belum ditambahkan");
  const container = $("#tentang .container").prepend(elDeskripsi, elVisi, elKepsek, elPost, elGallery);

  $("tentang-box", container).each(function (i) {
    if ((i + 1) % 2) {
      $(this).attr("data-aos", "fade-right");
    } else {
      $(this).attr("data-aos", "fade-left");
    }
  });

  const deskripsiLembaga = await data.find("deskripsi_lembaga", "value");
  const visiLembaga = await data.find("visi_lembaga", "value");
  const kepalaLembaga = await data.find("kepala_lembaga", "value");
  const kepalaLembagaSambutan = await data.find("kepala_lembaga_sambutan", "value");
  const kepalaLembagaImage = await data.find("kepala_lembaga_image", "value");
  const galeriLembaga = await data.find("galeri_lembaga", "value");

  if (deskripsiLembaga) elDeskripsi[0].fillContent = deskripsiLembaga;
  if (visiLembaga) elVisi[0].fillContent = visiLembaga;
  if (kepalaLembaga) {
    const imgCard = $("<image-card></image-card>")
      .attr("width", "230px")
      .attr("title-text", kepalaLembaga)
      .attr("title-position", "bottom")
      .attr("image", kepalaLembagaImage ?? "")
      .attr("modal-title", "Sambutan Kepala Sekolah");
    if (kepalaLembagaSambutan ?? "") imgCard.attr("modal-text", kepalaLembagaSambutan ?? "");
    elKepsek[0].fillAppend(imgCard[0]);
  }
  if (galeriLembaga) {
    const gallery = document.createElement("gallery-wrapper");
    gallery.addItems(galeriLembaga);
    elGallery[0].fillAppend(gallery);
  }
};

export default tentang;
