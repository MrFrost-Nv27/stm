import "../../components/jurusan/jurusan-list.js";
import DataJurusan from "../../data/data-jurusan.js";

const jurusan = () => {
  const listJurusan = document.createElement("jurusan-list");
  const container = $("#jurusan .container").prepend(listJurusan);
  DataJurusan.getAll().then((data) => {
    if (data.length > 0) {
      listJurusan.addItems(data);
    }
  });
};

export default jurusan;
