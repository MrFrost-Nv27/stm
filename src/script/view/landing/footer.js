import DataSosial from "../../data/data-sosial.js";
import "../../components/social/social-wrapper.js";

const footer = async (data) => {
  const elLogo = $("<footer-wrapper></footer-wrapper>").attr("no-title", true);
  const elMap = $("<footer-wrapper></footer-wrapper>").attr("title", "Lokasi");
  const elKontak = $("<footer-wrapper></footer-wrapper>").attr("title", "Kontak");
  const elMedsos = $("<footer-wrapper></footer-wrapper>").attr("title", "Media Sosial");
  const divKontak = $("<div></div>").append(elKontak).append(elMedsos);
  const elFooterLembaga = $(".footer-meta .lembaga");
  const container = $("footer .container").prepend(elLogo, elMap, divKontak);

  const namaLembaga = await data.find("nama_lembaga", "value");
  const logoLembaga = await data.find("logo_lembaga", "value");
  const alamatLembaga = await data.find("alamat_lembaga", "value");
  const lokasiLembaga = await data.find("lokasi_lembaga", "value");
  const tahunWeb = await data.find("tahun_web", "value");

  if (namaLembaga) {
    elLogo[0].addName(namaLembaga);
    elFooterLembaga[0].innertext = namaLembaga;
  }
  if (logoLembaga) elLogo[0].addLogo(logoLembaga);
  if (alamatLembaga) elLogo[0].addDescription(alamatLembaga);
  if (lokasiLembaga) elMap[0].addMap(lokasiLembaga);

  DataSosial.getAll().then((data) => {
    if (data.kontak.length > 0) {
      const kontak = $("<social-wrapper></social-wrapper>");
      kontak[0].addItems(data.kontak, false);
      elKontak[0].fillAppend(kontak[0]);
    }
    if (data.medsos.length > 0) {
      const medsos = $("<social-wrapper></social-wrapper>");
      medsos[0].addItems(data.medsos);
      elMedsos[0].fillAppend(medsos[0]);
    }
  });

  $(".copyright").text(function () {
    const createdYear = tahunWeb ?? 2023;
    const now = new Date().getFullYear();
    return createdYear == now ? createdYear : `${createdYear} - ${now}`;
  });
};

export default footer;
