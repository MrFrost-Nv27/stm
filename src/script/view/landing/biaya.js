import DataPay from "../../data/data-pay.js";
import "../../components/blank-data.js";

const biaya = (data) => {
  DataPay.getAll().then((pay) => {
    if (pay.ppdb.list.length > 0) {
      pay.ppdb.list.forEach((i, id) => {
        addItem(i, id + 1);
      });
      addTotal(pay.ppdb.total);
      $(".nominal").each(function (index) {
        const value = $(this).text();
        $(this).text(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
      });
    } else {
      const blank = $(`<blank-data></blank-data>`).attr("pesan", "Belum ada Data Pembayaran");
      $("#biaya .content-box").empty().append(blank);
    }
  });

  const addItem = (item = {}, num = 0) => {
    const index = $(`<th scope="row" class="number">${num}</th>`);
    const name = $(`<td><span data-bs-toggle="tooltip" data-bs-placement="right" data-bs-title="${item.keterangan}">${item.nama}</span></td>`);
    const nominal = $(`<td class="nominal">${item.nominal}</td>`);
    const el = $(`<tr></tr>`).append(index, name, nominal);
    document.querySelector("tbody").append(el[0]);
  };
  const addTotal = (total = 0) => {
    const el = $(`<tr><th scope="row" colspan="2">Total</th><td class="nominal total">${total}</td></tr>`);
    document.querySelector("tbody").append(el[0]);
  };
};

export default biaya;
